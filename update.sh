#!/bin/sh

docker login registry.gitlab.datalife.es

docker build -q --rm --no-cache -t registry.gitlab.datalife.es/datalife/docker-tryvy:latest .

docker push registry.gitlab.datalife.es/datalife/docker-tryvy:latest